<?php
/**
 * @file
 * Contains
 *   \Drupal\webform_submission_to_pdf\Form\WebformSubmissionToPdfConfigForm.
 */

namespace Drupal\webform_submission_to_pdf\Form;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class WebformSubmissionToPdfConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_submission_to_pdf_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('webform_submission_to_pdf.settings');
    $form['hours_link_validity'] = [
      '#type' => 'number',
      '#title' => $this->t('Hours of link validity'),
      '#default_value' => $config->get('webform_submission_to_pdf.hours_link_validity') ? $config->get('webform_submission_to_pdf.hours_link_validity') : '1',
      '#required' => TRUE,
    ];
    $form['logo_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width of the logo (css compatible)'),
      '#default_value' => $config->get('webform_submission_to_pdf.logo_width') ? $config->get('webform_submission_to_pdf.logo_width') : '42mm',
      '#required' => TRUE,
    ];
    $form['logo_src'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SRC of the logo'),
      '#default_value' => $config->get('webform_submission_to_pdf.logo_src') ? $config->get('webform_submission_to_pdf.logo_src') : '/themes/custom/mml/images/mml-logo_fi.png',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webform_submission_to_pdf.settings');
    $config->set('webform_submission_to_pdf.hours_link_validity', $form_state->getValue('hours_link_validity'))
      ->set('webform_submission_to_pdf.logo_width', $form_state->getValue('logo_width'))
      ->set('webform_submission_to_pdf.logo_src', $form_state->getValue('logo_src'))
      ->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'webform_submission_to_pdf.settings',
    ];
  }
}
