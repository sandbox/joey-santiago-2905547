<?php

namespace Drupal\webform_submission_to_pdf\Controller;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\webform\WebformRequestInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Controller\WebformSubmissionController;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides route responses for webform submissions.
 */
class WebformSubmissionToPdfController extends WebformSubmissionController {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * @var null|\Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a WebformSubmissionController object.
   *
   * @param \Drupal\webform\WebformRequestInterface $request_handler
   *   The webform request handler.
   */
  public function __construct(WebformRequestInterface $request_handler, ConfigFactoryInterface $config_factory, CurrentRouteMatch $current_route_match, RequestStack $request_stack) {
    parent::__construct($request_handler);
    $this->config = $config_factory->get('webform_submission_to_pdf.settings');
    $this->currentRouteMatch = $current_route_match;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('webform.request'),
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('request_stack')
    );
  }

  /**
   * Returns a webform submission in a specified format type.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   * @param string $type
   *   The format type.
   *
   * @return array
   *   A render array representing a webform submission in a specified format
   *   type.
   */
  public function show(WebformSubmissionInterface $webform_submission, $output_type) {
    // Disable caching on this page so that we can always perform the access
    // check.
    \Drupal::service('page_cache_kill_switch')->trigger();
    $source_entity = $this->requestHandler->getCurrentSourceEntity();
    // Submission.
    $build = [
      '#theme' => 'webform_submission_pdf',
      '#webform_submission' => $webform_submission,
      '#source_entity' => $source_entity,
      '#output_type' => $output_type,
    ];
    return $build;
  }

  /**
   * Allows access if uuid is set as parameter. The link is valid for one week.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Allowed or forbidden.
   */
  public function access(AccountInterface $account) {
    $source_entity = $this->requestHandler->getCurrentSourceEntity();
    if ($account->hasPermission('view own webform submission')) {
      return AccessResult::allowed();
    }
    else {
      /** For other users we need uuid set as param and the link is valid for
       * one week only (for now. Will be configurable later).
       */
      $param = $this->currentRouteMatch->getParameter('uuid');
      if (isset($param)) {
        $get_uuid = $param;
        if ($get_uuid == $source_entity->uuid()) {
          $time = time();
          $get_time = $source_entity->get('completed')->getString();
          $modify = ('+' . $this->config->get('webform_submission_to_pdf.hours_link_validity') . ' hours');
          $dateTime = new \Datetime();
          $dateTime->setTimestamp($get_time);
          $dateTime->modify($modify);
          if ($time <= $dateTime->getTimestamp()) {
            return AccessResult::allowed();
          }
        }
      }
      else {
        if ($this->request->getMethod() == "POST") {
          // Allow access for post because Webform calls this check upon
          // submission.
          return AccessResult::allowed();
        }
      }
      return AccessResult::forbidden();
    }
  }
}
