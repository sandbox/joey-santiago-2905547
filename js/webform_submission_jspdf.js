(function ($, Drupal) {
  Drupal.behaviors.webformSubmissionPDF = {
    attach: function (context, settings) {
      function outputFromHTML() {
        const element = document.getElementById("submission_output");
        html2pdf(element, {
          margin:       1,
          filename:     drupalSettings.webform_submission_to_pdf.jspdf.title+'.pdf',
          image:        { type: 'jpeg', quality: 1 },
          html2canvas:  { dpi: 192, letterRendering: true },
          jsPDF:        { unit: 'mm', format: 'a4', orientation: 'portrait' }
        });
      }
      $("#submission_output").once("preparePDF").each(function(){
        $("<img src='"+drupalSettings.webform_submission_to_pdf.jspdf.logo+"'/>").prependTo("#submission_output").css("width", drupalSettings.webform_submission_to_pdf.jspdf.logoSize);
        $("h1.page-title").remove().clone().prependTo("#submission_output");
      });
      $('#create_pdf', context).once('createPDF').click(function () {
        outputFromHTML();
      });
    }
  };
})(jQuery, Drupal);
