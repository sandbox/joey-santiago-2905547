The way this works is pretty simple:

It adds a route webform/WEBFORM_ID/submission/SUBMISSION_ID/pdf/SUBMISSION_UUID
and gives this a blank html.twig template which attempts to strip anything from
the page except from the webform submission. It adds then a button which uses JS
to create a pdf file for printing.
